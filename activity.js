
////////////#3//////////
// What directive is used by Node.js in loading the modules it needs?
require("<title.js>");
//What Node.js module contains a method for server creation?
http
//What is the method of the http object responsible for creating a server using Node.js?
createServer();
//What method of the response object allows us to set status codes and content types?
writeHead();
//Where will console.log() output its contents when run in Node.js?
Terminal
//What property of the request object contains the address' endpoint?
url()

//////////////#5////////////
let HTTP = require("http");
let port = 3000;
HTTP.createServer((request,response)=>{
	console.log("server is successfully running.");
	if(request.url=="/login"){
		response.write("you are in the login page");
        response.end();
	}else{
		response.writeHead(404,{"Content-Type":"text/plain"})
		response.write("ERROR: Page cannot be found!");
        response.end();
	}
}).listen(port);